package progressSoftEnduction9;
import org.apache.commons.validator.routines.EmailValidator;
import java.util.ArrayList;

public class UserManagement {

    private UserInfo user;
    private DataBase dataBase;
    private final char USERNAME = 'U';
    private final char EMAIL = 'E';

    UserManagement(DataBase dataBase){
        this.dataBase = dataBase;
    }

    public UserInfo getUser(){
        return user;
    }

    protected String getAllUsers() {
        // TODO no need to throw exception, just display an empty list
        if(dataBase.isEmptyDB())
           return "Empty List";
        // TODO no need
        //dataBase.isDataValid();
        else {
            ArrayList<UserInfo> usersList;
            usersList = dataBase.printUserList();
            String userList= "";
            for (UserInfo userInfo : usersList)
                userList += userInfo + "\n";
            return userList;
        }
    }

    protected void addNewUser(String newUser) {
        isInputValid(newUser);
        newUser = newUser.toLowerCase();
        String[] userInfo = newUser.split(",");
        validateIfEnter3Input(userInfo);
        validateEmailFormat(userInfo[2]);
        if (validateIfUnique(userInfo)) {
            isUserExists(userInfo);
            storeTheData();
        }
    }

    private void isInputValid(String userInput){
        if(userInput == null)
            throw new NullPointerException("null user data");
    }

    private void validateEmailFormat(String email){
        EmailValidator validator = EmailValidator.getInstance();
        if(!(validator.isValid(email)))
            throw new IllegalArgumentException("invalid email formatting!");
    }

    private boolean validateIfUnique(String []userInfo){
        return validateUsernameIfUnique(userInfo[0].replaceAll("\\s", ""))
                && validateEmailIfUnique(userInfo[2].replaceAll("\\s", ""));
    }

    // TODO both validate methods have similar code
    private boolean validateUsernameIfUnique(String Username){
        CopyCondition condition = getCopyCondition(USERNAME);
        return checkUserExistence(Username, condition, USERNAME);
    }

    private boolean validateEmailIfUnique(String email) {
        CopyCondition condition = getCopyCondition(EMAIL);
        return checkUserExistence(email, condition, EMAIL);
    }

    private boolean checkUserExistence(String dataToCheck, CopyCondition condition, char type){
        UserInfo userData = dataBase.searchForUser(dataToCheck, condition);
        if(userData == null)
            return true;
        else if(type == 'E')
            throw new IllegalArgumentException("email already exist!");
        else
            throw new IllegalArgumentException("user already exist!");
    }


    private void validateIfEnter3Input(String [] userInfo){
        if(userInfo.length < 3)
            throw new IllegalArgumentException("invalid input formatting!");
    }

    private void storeTheData(){
        dataBase.storeData(user);
    }

    private void isUserExists(String[] userInfo) {
        String username = userInfo[0];
        String name = userInfo[1];
        String email = userInfo[2];
        user = new UserInfo(username,name, email);
    }

    protected void enableUser(String username) {
        UserInfo userData = isUserExists(username);
        enableOrDisable(userData, true);
    }

    protected void disableUser(String username) {
        UserInfo userData = isUserExists(username);
        enableOrDisable(userData, false);
    }

    private UserInfo isUserExists(String username) {
        isInputValid(username);
        CopyCondition condition = getCopyCondition(USERNAME);
        return dataBase.searchForUser(username.toLowerCase(), condition);
    }

    private CopyCondition getCopyCondition(char copyType) {
        switch (copyType) {
            case 'U':
                return this::checkUserData;
            case 'E':
                return this::checkEmail;
            default:
                throw new IllegalArgumentException("unknown copy type: " + copyType);
        }
    }

    private boolean checkUserData(String username, UserInfo user) {
        return user.getUsername().equals(username);
    }

    private boolean checkEmail(String email, UserInfo user) {
        return user.geteMail().equals(email);
    }

    private void enableOrDisable(UserInfo user, boolean enableOrDisableFlag) {
        if(user == null)
            throw new IllegalArgumentException("username does not exist!");
        else if (user.getActive() && enableOrDisableFlag)
            throw new IllegalArgumentException("user already active");
        else if (!(user.getActive()) && !enableOrDisableFlag)
            throw new IllegalArgumentException("user already inactive");
        else{
            user.setActive(enableOrDisableFlag);
            updateUserInfo(user);
        }
    }

    protected String resetPassword(String username) {
        UserInfo userData = isUserExists(username);
        return setNewPassword(userData);
    };

    private String setNewPassword(UserInfo user){
        if(user == null)
            throw new IllegalArgumentException("username does not exist!");
        else {
            String OldPassword= user.getPassword(), NewPassword;
            do {
                user.setPassword();
                NewPassword = user.getPassword();
            }while(OldPassword.equals(NewPassword));
            updateUserInfo(user);
            return NewPassword;
        }
    }

    private void updateUserInfo(UserInfo user) {
        dataBase.updateData(user);
    }

}
