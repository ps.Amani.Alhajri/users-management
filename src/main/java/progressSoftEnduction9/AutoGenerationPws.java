package progressSoftEnduction9;

import java.util.Random;

public class AutoGenerationPws {

    public String getPassword(){
        return generatePassword();
    }

    private String generatePassword() {
        String password;
        String random5Digits, random3letters;
        random5Digits = getDigits();
        random3letters = getLetters();
        password = mixRandomChar(random5Digits, random3letters);
        return password;
    }

    private String getDigits(){
        char[] Digits = new char[5];
        for (int i = 0; i < 5; i++) {
            Random random = new Random();
            int randInt = random.nextInt(10);
            char randChar =  (char)(randInt + '0');
            randChar = getNonDuplicateChar("0123456789", Digits, randChar);
            Digits[i] = randChar;
        }
        return String.copyValueOf(Digits);
    }

    private String getLetters(){
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz";
        char[] letters = new char[3];
        for (int i = 0; i < 3; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            char randChar = AlphaNumericString.charAt(index);
            randChar = getNonDuplicateChar(AlphaNumericString, letters, randChar);
            letters[i] = randChar;
        }
        return String.copyValueOf(letters);
    }

    private char getNonDuplicateChar(String alphaNumericString, char[] letters, char randChar) {
        int index;
        while (String.valueOf(letters).contains(String.valueOf(randChar))) {
            index = (int)(alphaNumericString.length() * Math.random());
            randChar = alphaNumericString.charAt(index);
        }
        return randChar;
    }

    private String mixRandomChar(String random5Digits,String random3letters){
        StringBuilder result = new StringBuilder();
        Random random = new Random();
        boolean startWithDigitOrLetter = random.nextBoolean();
        for (int i = 0; i < random5Digits.length() || i < random3letters.length(); i++) {
                if (i < random5Digits.length())
                    result.append(random5Digits.charAt(i));
                if (i < random3letters.length())
                    result.append(random3letters.charAt(i));
        }
        return result.toString();
    }
}
