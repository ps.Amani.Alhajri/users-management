package progressSoftEnduction9;

import java.util.ArrayList;

public class InMemoryDatabase implements DataBase {

    private ArrayList<UserInfo> userList;

    InMemoryDatabase() {
        userList = new ArrayList<>();
    }

    InMemoryDatabase(UserInfo newUser) {
        userList = new ArrayList<>();
        storeData(newUser);
    }

    @Override
    public void storeData(UserInfo newUser) {
        // TODO prevent the null user in here
        isNull(newUser);
        if (isUserExist(newUser))
            throw new IllegalArgumentException("user already exist!");
        userList.add(newUser);
    }

    @Override
    public ArrayList<UserInfo> getData() {
        // TODO return a copy of the list
        return new ArrayList<>(userList);
    }

    @Override
    public void deleteData() {
        userList.clear();
    }

    @Override
    public void updateData(UserInfo user) {
        // TODO prevent the null user in here
        isNull(user);
        // TODO search for the user then update it
        userList.set(getIndexOfUser(user), user);
    }

    @Override
    public boolean isEmptyDB() {
        return userList.isEmpty();
    }

    @Override
    public ArrayList<UserInfo> printUserList() {
        // TODO data access object is not responsible for printing
        // TODO I only print on console, what to do if someone needs to change it, to something else!!!
        return new ArrayList<>(userList);
    }

    @Override
    public UserInfo searchForUser(String dataToCheck, CopyCondition condition) {
        for (int i = 0; i < userList.size(); i++) {
            UserInfo user = userList.get(i);
            if (condition.doCopy(dataToCheck, user)) {
                // TODO remove the index, no need
                // setIndex(i);
                return user;
            }
        }
        return null;
    }

    private boolean isUserExist(UserInfo user) {
        return userList.contains(user);
    }

    private int getIndexOfUser(UserInfo user) {
        for (int i = 0; i < userList.size(); i++) {
            UserInfo realUser = userList.get(i);
            if (realUser.getUsername().equals(user.getUsername()))
                return i;
        }
        throw new IllegalArgumentException("user dose not exist!");
    }

    private void isNull(UserInfo newUser) {
        if(newUser == null)
            throw new NullPointerException("null user date");
    }

}
