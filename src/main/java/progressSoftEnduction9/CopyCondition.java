package progressSoftEnduction9;

public interface CopyCondition {
    boolean doCopy(String username, UserInfo user);
}
