package progressSoftEnduction9;
import java.util.ArrayList;

public interface DataBase {
    public void storeData(UserInfo newUser);
    public ArrayList<UserInfo> getData();
    public void deleteData();
    public void updateData(UserInfo user);
    public ArrayList<UserInfo> printUserList();
    public UserInfo searchForUser(String dataToCheck, CopyCondition condition);
    public boolean isEmptyDB();
}
