package progressSoftEnduction9;


public class UserInfo {
    private String name;
    private String username;
    private String eMail;
    private String password;
    private boolean active;

    UserInfo(String username1, String name1, String eMail1) {
        name = name1.toLowerCase();
        eMail = eMail1.replaceAll("\\s+", "").toLowerCase();
        eMail = eMail.toLowerCase();
        username = username1.replaceAll("\\s+", "").toLowerCase();
        username = username.toLowerCase();
        active = true;
        password = autoGenerationPassword();
    }

    public String getName() {
        return name;
    }


    public String getUsername() {
        return username;
    }


    public String geteMail() {
        return eMail;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword() {
        this.password = autoGenerationPassword();
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private String autoGenerationPassword() {
        AutoGenerationPws newPass = new AutoGenerationPws();
        return newPass.getPassword();
    }

    public String toString() {
        String userInfo = "", activeStr;
        if (active)
            activeStr = "Yes";
        else
            activeStr = "No";

        userInfo = String.format("|%-15s|%-15s|%-15s|%-15s|", username, name, eMail, activeStr);
        return userInfo;
    }

}
