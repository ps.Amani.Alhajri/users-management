package progressSoftEnduction9;

import java.util.Scanner;

public class IOThroughConsole {

    private UserManagement userManagementSystem;

    IOThroughConsole(UserManagement userManagementSystem){
        this.userManagementSystem = userManagementSystem;
    }

    public void getMenuChoice(){
        char choice;
        do {
            printMenu();
            Scanner userInput = new Scanner(System.in);
            choice = userInput.next().charAt(0);
        }while(checkUserInput(choice));
    }

    // TODO an option consist of a number, description, and an action to be executed when chosen
    // try to encapsulate those properties in one class
    private void printMenu(){
        System.out.println("1- Display all users\n" +
                "2- Add new user\n" +
                "3- Enable user\n" +
                "4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\n" +
                "Please enter your option:");
    }

    public boolean checkUserInput(char choice) {
        switch (choice){
            case '1':
                displayAllUsers();
                return true;
            case '2':
                getNewUserInfo();
                return true;
            case '3':
                promptUserNameToEnable();
                return true;
            case '4':
                promptUserNameToDisable();
                return true;
            case '5':
                promptUserNameToResetPassword();
                return true;
            case '6':
                return false;
        }
        throw new IllegalArgumentException("invalid input, You have to choose from 1-6!");
    }

    private void displayAllUsers() {
        System.out.println("+---------------+--------------+----------------+--------------+");
        System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|","Username", "Name", "E-Mail", "Active (yes/no)" ));
        userManagementSystem.getAllUsers();
        System.out.println("+---------------+--------------+----------------+--------------+");
    }

    private void getNewUserInfo(){
        String userInfo = promptUserForInfo();
        userManagementSystem.addNewUser(userInfo);
        UserInfo user = userManagementSystem.getUser();
        printUserInfo(user);
    }

    private void printUserInfo(UserInfo user){
        System.out.println("Name: " + user.getName());
        System.out.println("Username: " + user.getUsername());
        System.out.println("E-Mail: " + user.geteMail());
        System.out.println("Password: " + user.getPassword());
    }

    private void promptUserNameToEnable(){
        String username = getUsername();
        userManagementSystem.enableUser(username);
    }

    private void promptUserNameToDisable(){
        String username = getUsername();
        userManagementSystem.disableUser(username);
    }

    private void promptUserNameToResetPassword(){
        String username = getUsername();
        String NewPassword =  userManagementSystem.resetPassword(username);
        System.out.println("new Password: " + NewPassword);
    }

    private String promptUserForInfo(){
        System.out.println("Enter user information in the format (username, name, email)");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

    private String getUsername(){
        System.out.println("Please enter the username: ");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine();
    }

}
