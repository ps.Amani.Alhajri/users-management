package progressSoftEnduction9;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsersManagementTest {


    @Test
    public void givenInvalidChoice_whenGetUserChoiceFromMenu_thenThrowIllegalArgumentException() {
        char choice = 7;
        DataBase dataBase = new InMemoryDatabase();
        UserManagement us = new UserManagement(dataBase);
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new IOThroughConsole(us).checkUserInput(choice));
        Assertions.assertEquals("invalid input, You have to choose from 1-6!", thrown.getMessage());
    }

    @Test
    public void givenNotExistUserInfo_whenUpdateData_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Salim", "aa@dd.com");
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new InMemoryDatabase().updateData(user));
        Assertions.assertEquals("user dose not exist!", thrown.getMessage());
    }

    @Test
    public void givenExistUserInfo_whenUpdateData_thenThrowNullPointerException() {
        UserInfo user = null;
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new InMemoryDatabase().updateData(user));
        Assertions.assertEquals("null user date", thrown.getMessage());
    }

    @Test
    public void givenNotExistUserInfo_whenStoreData_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Salim", "aa@dd.com");
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new InMemoryDatabase(user).storeData(user));
        Assertions.assertEquals("user already exist!", thrown.getMessage());
    }

    @Test
    public void givenExistUserInfo_whenStoreData_thenThrowNullPointerException() {
        UserInfo user = null;
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new InMemoryDatabase().storeData(user));
        Assertions.assertEquals("null user date", thrown.getMessage());
    }

    @Test
    public void givenNullData_whenDisplayAllUsers_thenPrintEmptyList() {
        DataBase db = new InMemoryDatabase();
        UserManagement us = new UserManagement(db);
        String actualString = us.getAllUsers();
        Assertions.assertEquals("Empty List", actualString);
    }

    @Test
    public void givenNullData_whenAddNewUser_thenThrowNullPointerException() {
        String UserInfo = null;
        DataBase db = new InMemoryDatabase();
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new UserManagement(db).addNewUser(UserInfo));
        Assertions.assertEquals("null user data", thrown.getMessage());
    }

    @Test
    public void givenInvalidInputFormat_whenAddNewUser_thenThrowIllegalArgumentException() {
        String UserInfo = "Dr Amani Aman.com";
        DataBase db = new InMemoryDatabase();
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).addNewUser(UserInfo));
        Assertions.assertEquals("invalid input formatting!", thrown.getMessage());
    }

    @Test
    public void givenExistUserName_whenAddNewUser_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Salim", "aa@dd.com");
        String newUserInfo = "Dr, Amani,amani@gmail.com";
        DataBase db = new InMemoryDatabase();
        db.storeData(user);
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).addNewUser(newUserInfo));
        Assertions.assertEquals("user already exist!", thrown.getMessage());
    }

    @Test
    public void givenExistEmail_whenAddNewUser_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Amani", "aa@dd.com");
        String newUserInfo = "Admin,Mohammed, aa@dd.com";
        DataBase db = new InMemoryDatabase();
        db.storeData(user);
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).addNewUser(newUserInfo));
        Assertions.assertEquals("email already exist!", thrown.getMessage());
    }

    @Test
    public void givenInvalidEmailAddress_whenAddNewUser_thenThrowIllegalArgumentException() {
        String UserInfo = "Dr, Amani, Aman.com";
        DataBase db = new InMemoryDatabase();
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).addNewUser(UserInfo));
        Assertions.assertEquals("invalid email formatting!", thrown.getMessage());
    }

    @Test
    public void givenNullUsername_whenEnableUser_thenThrowNullPointerException() {
        String Username = null;
        DataBase db = new InMemoryDatabase();
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new UserManagement(db).enableUser(Username));
        Assertions.assertEquals("null user data", thrown.getMessage());
    }

    @Test
    public void givenInvalidUserName_whenEnableUser_thenThrowIllegalArgumentException() {
        String Username = "Amaa";
        DataBase db = new InMemoryDatabase();
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).enableUser(Username));
        Assertions.assertEquals("username does not exist!", thrown.getMessage());
    }

    @Test
    public void givenUsernameAlreadyActive_whenEnableUser_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Amani", "aa@dd.com");
        String newUserInfo = "Dr";
        DataBase db = new InMemoryDatabase();
        db.storeData(user);
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).enableUser(newUserInfo));
        Assertions.assertEquals("user already active", thrown.getMessage());
    }

    @Test
    public void givenNullUsername_whenDisableUser_thenThrowNullPointerException() {
        String Username = null;
        DataBase db = new InMemoryDatabase();
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new UserManagement(db).disableUser(Username));
        Assertions.assertEquals("null user data", thrown.getMessage());
    }

    @Test
    public void givenInvalidUserName_whenDisableUser_thenThrowIllegalArgumentException() {
        String Username = "Amaa";
        DataBase db = new InMemoryDatabase();
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).disableUser(Username));
        Assertions.assertEquals("username does not exist!", thrown.getMessage());
    }

    @Test
    public void givenUsernameAlreadyActive_whenDisableUser_thenThrowIllegalArgumentException() {
        UserInfo user = new UserInfo("Dr", "Amani", "aa@dd.com");
        user.setActive(false);
        String newUserInfo = "DR";
        DataBase db = new InMemoryDatabase();
        db.storeData(user);
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).disableUser(newUserInfo));
        Assertions.assertEquals("user already inactive", thrown.getMessage());
    }

    @Test
    public void givenNullUsername_whenResetPassword_thenThrowNullPointerException() {
        String Username = null;
        DataBase db = new InMemoryDatabase();
        NullPointerException thrown = Assertions
                .assertThrows(NullPointerException.class, () -> new UserManagement(db).resetPassword(Username));
        Assertions.assertEquals("null user data", thrown.getMessage());
    }

    @Test
    public void givenInvalidUserName_whenResetPassword_thenThrowIllegalArgumentException() {
        String Username = "Amaa";
        DataBase db = new InMemoryDatabase();
        IllegalArgumentException thrown = Assertions
                .assertThrows(IllegalArgumentException.class, () -> new UserManagement(db).resetPassword(Username));
        Assertions.assertEquals("username does not exist!", thrown.getMessage());
    }

}
